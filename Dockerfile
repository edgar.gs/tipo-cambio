FROM openjdk:14-alpine
COPY build/libs/tipo-cambio-*-all.jar tipo-cambio.jar
EXPOSE 8080
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-jar", "tipo-cambio.jar"]