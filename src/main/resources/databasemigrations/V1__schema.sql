
DROP TABLE IF EXISTS TIPO_CAMBIO;

CREATE TABLE TIPO_CAMBIO (
  id    BIGINT SERIAL PRIMARY KEY NOT NULL,
  moneda CHAR(10)              NOT NULL,
  valor DECIMAL(20, 2)              NOT NULL
);

INSERT INTO TIPO_CAMBIO(moneda, valor) VALUES('USD',3.51);