package tipo.cambio.dto;

import io.micronaut.core.annotation.Introspected;

@Introspected
public class TipoCambio {

    String monedaOrigen;
    String monedaDestino;
    double monto;
    double montoTipoCambio;
    double tipoCambio;

    public String getMonedaOrigen() {
        return monedaOrigen;
    }

    public void setMonedaOrigen(String monedaOrigen) {
        this.monedaOrigen = monedaOrigen;
    }

    public String getMonedaDestino() {
        return monedaDestino;
    }

    public void setMonedaDestino(String monedaDestino) {
        this.monedaDestino = monedaDestino;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public double getMontoTipoCambio() {
        return montoTipoCambio;
    }

    public void setMontoTipoCambio(double montoTipoCambio) {
        this.montoTipoCambio = montoTipoCambio;
    }

    public double getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }
}
