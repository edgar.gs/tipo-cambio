package tipo.cambio.service;

import io.reactivex.Maybe;
import io.reactivex.Single;
import tipo.cambio.dto.TipoCambio;
import tipo.cambio.modelo.Moneda;
import tipo.cambio.repository.MonedaRepository;

import javax.inject.Singleton;

@Singleton
public class MonedaService {

    private MonedaRepository monedaRepository;

    public MonedaService(MonedaRepository monedaRepository) {
        this.monedaRepository = monedaRepository;
    }

    public Maybe<TipoCambio> consultarTipoCambio(TipoCambio tipoCambio) {
        Single<Moneda> moneda = monedaRepository.findByCodigo(tipoCambio.getMonedaDestino());
        return moneda.flatMapMaybe(item -> {
                tipoCambio.setTipoCambio(item.getValor());
                tipoCambio.setMontoTipoCambio(Math.round((item.getValor()*tipoCambio.getMonto())*100)/100);
                return Maybe.just(tipoCambio);
                });
    }
}
