package tipo.cambio.repository;

import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.CrudRepository;
import io.reactivex.Single;
import tipo.cambio.modelo.Moneda;

@JdbcRepository(dialect = Dialect.H2)
public interface MonedaRepository extends CrudRepository<Moneda,Long> {

    Single<Moneda> findByCodigo(String codigo);
}
