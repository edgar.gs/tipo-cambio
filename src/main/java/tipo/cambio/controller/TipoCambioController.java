package tipo.cambio.controller;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.reactivex.Maybe;
import tipo.cambio.dto.TipoCambio;
import tipo.cambio.service.MonedaService;

@Controller("/")
public class TipoCambioController {

    private MonedaService monedaService;

    public TipoCambioController(MonedaService monedaService) {
        this.monedaService = monedaService;
    }

    @Get("/tipoCambio{?args*}")
    public Maybe<TipoCambio> consultaTipoCambio(TipoCambio args){
        return monedaService.consultarTipoCambio(args);
    }
}
